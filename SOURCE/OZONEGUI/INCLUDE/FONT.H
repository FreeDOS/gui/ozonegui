/**
*	Font manipulation extension
*/

#ifndef _FONT_H_INCLUDED_
#define _FONT_H_INCLUDED_

_PUBLIC FONT*  FontLoad ( l_text szFilename );

/**
*	Global main system font
*/
extern FONT* default_font;

#endif /* _FONT_H_INCLUDED_ */
