////////////////////////////////////////////////////////////////////////////////
//
//  Console - Header File
//
//  (c) Copyright 2004 Point Mad. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _CONSOLE_H_INCLUDED_
#define _CONSOLE_H_INCLUDED_

//typedef struct TConsole *PConsole; // Defined in dynld.h

typedef struct TConWdg
{
	struct TWidget w;
	
	PConsole o;
} TConWdg, *PConWdg;

#define CONWDG(o) ((PConWdg)(o))

typedef struct TConsole
{
	l_uchar Color;
	l_long   flags;
	l_long   cursor;
	PWindow w;
	PConWdg box;
	PTimer t;
	PTimer t1;
	char VideoMem[24][80];
	char VideoMem1[24][80];
	char *cur;
	char *cur1;
	
	char KeyBuffer[256];
	l_long KeyNb;
	
	PApplication App; // An application can only open one console.
	
} TConsole;

#define CONSOLE(o) ((PConsole)(o))

#endif
