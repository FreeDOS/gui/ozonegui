#ifndef _SLIDER_H_INCLUDED_
#define _SLIDER_H_INCLUDED_
#include "widget.h"

#define SLIDER(w)	((PSlider)(w))

/**
*	Messages sent upon move of the slider
*/
#define SL_SCROLLUP		0x00FF1100
#define SL_SCROLLDOWN	0x00FF1200

typedef struct TSlider *PSlider;
typedef struct TSlider
{
	struct	TWidget o;

	l_ulong	Value;

	l_ulong	Steps;
	l_char State;

} TSlider;

PSlider CreateSlider( PApplication App, TRect r, l_ulong Steps );

#endif /* _SLIDER_H_INCLUDED_ */
