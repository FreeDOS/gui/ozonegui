////////////////////////////////////////////////////////////////////////////////
//
//	Resource files - Core file
//
//	(c) Copyright 2003 Point Mad, Lukas Lipka. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////
#include "kernel.h"
#include "dms.h"

PDataMan GetDataManager ( l_datatype Type );

PList DMSLoadRessource ( PFile f, l_ulong Entrys ) {
  PList Lst = NewList();
  TResEntry E;
  PCodec  M;
  l_ulong   O;
  if ( !Lst ) return NULL;
  #ifdef TEST
    DebugMessage ("LoadRessource :: Starting loading 0x%08x from file 0x%08x",Lst,f);
  #endif
  while ( Entrys ) {
    FileRead(&E,sizeof(TResEntry),1,f);
    M = FoundCodecByEncoder(E.DataType,E.DataEncode);
	  if ( M ) {
		  void *Data;
		  if ( M->Load(f,&Data,&(E.DataSize),&O) ) {
			  PListItem i;
			  PDataMan m = GetDataManager(E.DataType);
			  void *fr = NULL;
			  if ( m ) fr = m->FreeData;
		    i =  NewListItemEx(E.Name,Data,fr,E.DataType,E.DataSize);
		    ListAddItem(Lst,i);
		    #ifdef TEST
		      DebugMessage ("LoadRessource :: 0x%08x :: %d '%s' loaded, %s",Lst,Entrys,E.Name,Data);
		    #endif
	    } else
	      DebugError ("LoadRessource :: 0x%08x :: DataType/DataEncode do not support loading from ressource file ! : 0x%08x/0x%08x",Lst,E.DataType,E.DataEncode);
	  } else
	    DebugError ("LoadRessource :: 0x%08x :: Unknow DataType/DataEncode : 0x%08x/0x%08x",Lst,E.DataType,E.DataEncode);
	  Entrys--;
  }
  return Lst;
}

void DMSWriteRessource ( PFile f, PList Lst, l_ulong *Entrys ) {
	PListItem a,b;

	TResEntry E;
  PCodec  M;
  l_ulong   O, St, En;
  #ifdef TEST
	  DebugMessage("WriteRessource :: 0x%08x :: START",Lst);
	#endif
	(*Entrys) = 0;
	if ( !Lst->Last ) return;

	a = b = Lst->Last->Next;

	do {
		M = DefaultRessourceCodec(a->DataType);
		if ( M ) {
			if ( M->Save ) {
				memset(&E,0,sizeof(TResEntry));
			  E.DataType = M->DataTypeManaged;
			  E.DataEncode = M->DataEncodeManaged;
			  memcpy(&E.Name,a->Key,min(31,TextLen(a->Key)));

			  FileGetPos(f,&En);
			  FileWrite(&E,sizeof(TResEntry),1,f);
			  FileGetPos(f,&St);
				if ( M->Save(f,a->Data,a->DataSize,&O) ) {
				  E.DataSize = O-St;
				  #ifdef TEST
				    DebugMessage("WriteRessource :: Wrote... %d %d 0x%08x 0x%08x '%s', %s",E.DataSize,a->DataSize,E.DataType,E.DataEncode,E.Name, a->Data);
			    #endif
				  //fsetpos(f,&En);
			    FileSeek(f, En, SEEK_SET);
			    FileWrite(&E,sizeof(TResEntry),1,f);
			    //fsetpos(f,&O);
			    FileSeek(f, O, SEEK_SET);
		    } else
		      DebugError ("WriteRessource :: 0x%08x :: ERROR WRITTING TO FILE !!",Lst);
		  } else
        DebugError ("WriteRessource :: 0x%08x :: Default DataType dot not support write : 0x%08x",Lst,a->DataType);
    } else
      DebugError ("WriteRessource :: 0x%08x :: DataType not supported : 0x%08x",Lst,a->DataType);


		(*Entrys)++;
		a = a->Next;
	} while ( a != b );
	#ifdef TEST
	  DebugMessage("WriteRessource :: 0x%08x :: END",Lst);
  #endif
}

l_bool SaveRessourceFile ( l_text File, PList Data  )
{
	PFile f;
	l_text a;
	TResHead Head;
	
	if ( a = TextRChr(File,'.') ) {
		if ( !TextCaseCompare(a,".app") || !TextCaseCompare(a,".dl") ) {
			TDynLdHeader DynHead;
			f = FileOpen(File,"rb+");
			if ( !f ) return false;
			FileRead(&DynHead, 1, sizeof(TDynLdHeader), f);
			if ( DynHead.Magic != ULONG_ID('D','n','L','d') ) {
				FileClose(f);
				DebugError("DYNLD/RESSOURCE :: Application is not a valid DynLD.");
				return false;
			}
			if ( DynHead.FileFormatVersion > DYNLDVERSION ) {
				FileClose(f);
				DebugError("DYNLD/RESSOURCE :: Application has invalid version number of DynLD linker.");
				return false;
			}
			FileSetPos(f,&(DynHead.RessourceOffset));
			DMSWriteRessource(f,Data,&(DynHead.RessourceEntries));
			FileSeek(f, 0, SEEK_SET);
			FileWrite(&DynHead,sizeof(TDynLdHeader),1,f);
			FileClose(f);
			return true;
		}
	}
	
	f = FileOpen(File,"wb");
	if ( !f ) return false;
	
	Head.Magic = ULONG_ID('D','M','S','R');
	Head.FormatVersion = ULONG_ID(0,0,0,1);
	FileWrite(&Head,sizeof(TResHead),1,f);

	DMSWriteRessource(f,Data,&Head.Entries);

	FileSeek(f, 0, SEEK_SET);
	FileWrite(&Head,sizeof(TResHead),1,f);
	FileClose(f);

	return true;
}

PList LoadRessourceFile ( l_text File )
{
	l_text a;
	PList Data = NULL;
	TResHead Head;
	PFile f = FileOpen(File, "rb");
	
	if ( !f ) return NULL;
	
	if ( a = TextRChr(File,'.') ) {
		if ( !TextCaseCompare(a,".app") || !TextCaseCompare(a,".dl") ) {
			TDynLdHeader DynHead;
			FileRead(&DynHead, 1, sizeof(TDynLdHeader), f);
			if ( DynHead.Magic != ULONG_ID('D','n','L','d') ) {
				FileClose(f);
				DebugError("DYNLD/RESSOURCE :: Application is not a valid DynLD.");
				return NULL;
			}
			if ( DynHead.FileFormatVersion > DYNLDVERSION ) {
				FileClose(f);
				DebugError("DYNLD/RESSOURCE :: Application has invalid version number of DynLD linker.");
				return NULL;
			}
			FileSetPos(f,&(DynHead.RessourceOffset));
			Data = DMSLoadRessource(f,DynHead.RessourceEntries);
			FileClose(f);
			return Data;
		}
	}

	FileRead(&Head,sizeof(TResHead),1,f);
	if ( Head.Magic == ULONG_ID('D','M','S','R') )
	{
		if ( Head.FormatVersion == ULONG_ID(0,0,0,1) )
		{
			Data = DMSLoadRessource(f,Head.Entries);
		}
	}

	FileClose(f);

	return Data;
}
