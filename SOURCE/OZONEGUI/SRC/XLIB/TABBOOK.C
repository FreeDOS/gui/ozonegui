////////////////////////////////////////////////////////////////////////////////
//
//  Tabbook - Core File
//
//	(c) Copyright 2003,2004 Point Mad, Lukas Lipka. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////

#include"kernel.h"
#include"tabbook.h"
#include"grfx.h"


l_ulong AppVersion = ULONG_ID(0,0,0,1);
char    AppName[] = "TabBook widget";
l_uid	nUID = "tabbook";
l_uid NeededLibs[] = { "widget","grfx", "skin","" };

void  TabBookDraw ( PWidget o, p_bitmap buffer, PRect w ) {
	l_int x = o->Absolute.a.x+2-TABBOOK(o)->Pos, y = o->Absolute.a.y;
	rectfill(buffer,w->a.x,w->a.y,w->b.x,w->b.y,COL_3DFACE);	
		
	vline(buffer,o->Absolute.a.x,o->Absolute.a.y+20,o->Absolute.b.y,COL_3DLIGHT);
	vline(buffer,o->Absolute.b.x,o->Absolute.a.y+20,o->Absolute.b.y,COL_3DDARK);
	hline(buffer,o->Absolute.a.x,o->Absolute.b.y,o->Absolute.b.x,COL_3DDARK);
	hline(buffer,o->Absolute.a.x,o->Absolute.a.y+19,o->Absolute.b.x,COL_3DLIGHT);

	
	if ( o->Last ) {
		PWidget a = o->Last;
		PWidget b = a;

		do {
			RectAssign(&TAB(a)->BtCache,x,y+1,x+text_length(default_font,TAB(a)->Title)+6,y+18);

			
			if ( TAB(a)->BtCache.b.x  < o->Absolute.b.x-20 ) {
			
				textout(buffer, default_font, TAB(a)->Title,x+3,y+2, COL_3DTEXT);
				
				if ( TAB(a) == TABBOOK(o)->Current ) {
					hline(buffer,TAB(a)->BtCache.a.x-1,TAB(a)->BtCache.a.y-1,TAB(a)->BtCache.b.x+1,COL_3DLIGHT);
					vline(buffer,TAB(a)->BtCache.a.x-1,TAB(a)->BtCache.a.y-1,TAB(a)->BtCache.b.y,COL_3DLIGHT);
					vline(buffer,TAB(a)->BtCache.b.x+1,TAB(a)->BtCache.a.y-1,TAB(a)->BtCache.b.y,COL_3DDARK);
					hline(buffer,TAB(a)->BtCache.a.x,o->Absolute.a.y+19,TAB(a)->BtCache.b.x,COL_3DFACE);
				} else {
					hline(buffer,TAB(a)->BtCache.a.x+1,TAB(a)->BtCache.a.y,TAB(a)->BtCache.b.x,COL_3DLIGHT);
					if ( TAB(a->Next) != TABBOOK(o)->Current || a == b ) vline(buffer,TAB(a)->BtCache.a.x,TAB(a)->BtCache.a.y,TAB(a)->BtCache.b.y,COL_3DLIGHT);
					if ( TAB(a->Prev) != TABBOOK(o)->Current || a->Prev == b )vline(buffer,TAB(a)->BtCache.b.x,TAB(a)->BtCache.a.y,TAB(a)->BtCache.b.y,COL_3DDARK);
				}
			
			} else if ( x  < o->Absolute.b.x-20 ) {
				
				l_int w = o->Absolute.b.x-20-x;
				
				if ( w > 3 ) DrawNiceText(buffer, default_font,x+3,y+2, w-3, TAB(a)->Title, COL_3DTEXT);
				
				if ( TAB(a) == TABBOOK(o)->Current ) {
					hline(buffer,x,y,o->Absolute.b.x-20,COL_3DLIGHT);
					vline(buffer,x,y,y+18,COL_3DLIGHT);
					//vline(buffer,o->Absolute.b.x-20,y,y+18,COL_3DDARK);
					hline(buffer,x+1,o->Absolute.a.y+19,o->Absolute.b.x-20,COL_3DFACE);
				} else {	
					hline(buffer,x+1,y+1,o->Absolute.b.x-20,COL_3DLIGHT);
					if ( TAB(a->Next) != TABBOOK(o)->Current || a == b ) 
						vline(buffer,x,y+1,y+18,COL_3DLIGHT);
					/*if ( TAB(a->Prev) != TABBOOK(o)->Current || a->Prev == b )
						vline(buffer,o->Absolute.b.x-20,y+1,y+18,COL_3DDARK);*/
				}
			}
			
			
			x = TAB(a)->BtCache.b.x + 1;
	
			a = a->Prev;
		} while ( a != b );		
		
		TABBOOK(o)->Max = x-o->Absolute.a.x-2+TABBOOK(o)->Pos;
		
		if ( TABBOOK(o)->Max > o->Absolute.b.x-22-o->Absolute.a.x ) {
			if ( TABBOOK(o)->Flags & TBF_LDWN )
				Rect3D(buffer,o->Absolute.b.x-19,o->Absolute.a.y,o->Absolute.b.x-10,o->Absolute.a.y+18,COL_3DDARK,COL_3DLIGHT);
			else
				Rect3D(buffer,o->Absolute.b.x-19,o->Absolute.a.y,o->Absolute.b.x-10,o->Absolute.a.y+18,COL_3DLIGHT,COL_3DDARK);
				
			if ( TABBOOK(o)->Flags & TBF_RDWN )
				Rect3D(buffer,o->Absolute.b.x-9,o->Absolute.a.y,o->Absolute.b.x,o->Absolute.a.y+18,COL_3DDARK,COL_3DLIGHT);
			else
				Rect3D(buffer,o->Absolute.b.x-9,o->Absolute.a.y,o->Absolute.b.x,o->Absolute.a.y+18,COL_3DLIGHT,COL_3DDARK);

			textout(buffer, default_font, "<",o->Absolute.b.x-17,o->Absolute.a.y+2, COL_3DTEXT);
			textout(buffer, default_font, ">",o->Absolute.b.x-7,o->Absolute.a.y+2, COL_3DTEXT);
		
		} 

	}
	
	
	
}
////////////////////////////////////////////////////////////////////////////////
void TabBookSelectTab ( PTabBook o, PTab t ) {
	if ( o->Current ) WIDGET(o->Current)->Flags &= ~WFVisible;
	o->Current = t;
	WIDGET(o->Current)->Flags |= WFVisible;
	WidgetDrawAll(WIDGET(o));
}
////////////////////////////////////////////////////////////////////////////////
void TabBookRemoveTab ( PTabBook o, PTab t ) {
	RemoveWidget(WIDGET(o),WIDGET(t));
}
////////////////////////////////////////////////////////////////////////////////
void TabBookDisposeTab ( PTabBook o, PTab t ) {
	WidgetDispose(WIDGET(t));
}
////////////////////////////////////////////////////////////////////////////////
PTab TabBookAddTab ( PTabBook o, l_text Title, p_bitmap Icon ) {
	TRect r;
	PTab t = (PTab)malloc(sizeof(TTab));
	if ( !t ) return NULL;
	memset(t,0,sizeof(TTab));
		
	r = WIDGET(o)->Relative;
	RectAssign(&r,3,23,r.b.x-r.a.x-3,r.b.y-r.a.y-3);
	
	IntialiseWidget(WIDGET(o)->AppOwner, WIDGET(t), r, "Tab");
	
	WIDGET(t)->Flags &= ~WFVisible;
	WIDGET(t)->BackgroundColor = COL_3DFACE; 
	InsertWidget(WIDGET(o),WIDGET(t));
	
	t->Title = TextDup(Title);
	
	return t;
	
}
////////////////////////////////////////////////////////////////////////////////
l_bool  TabBookEventHandler ( PWidget o, PEvent Ev )
{
	if (Ev->Type == EV_MOUSE)
	{
		if (Ev->Message == WEvMouseLDown)
		{
			l_ulong w = o->Absolute.b.x-22-o->Absolute.a.x;
			
			if ( (TABBOOK(o)->Max > w) && (Mouse->State.p.y < o->Absolute.a.y+20) && (Mouse->State.p.x > o->Absolute.b.x-20) ) {
				if ( Mouse->State.p.x < o->Absolute.b.x-10 ) {
					TABBOOK(o)->Flags |= TBF_LDWN;
				} else {
					TABBOOK(o)->Flags |= TBF_RDWN;
				}
				
				WidgetDraw(o,NULL);
				return true;
			}
		}		
		
		if (Ev->Message == WEvMouseLUp)
		{
			l_ulong w = o->Absolute.b.x-22-o->Absolute.a.x;
			
			if ( (TABBOOK(o)->Max > w) && (Mouse->State.p.y < o->Absolute.a.y+20) && (Mouse->State.p.x > o->Absolute.b.x-20) ) {
				if ( Mouse->State.p.x < o->Absolute.b.x-10 ) {
					if ( TABBOOK(o)->Pos > 0 ) 
						TABBOOK(o)->Pos -= 10;
				} else {
					if ( TABBOOK(o)->Pos < TABBOOK(o)->Max-w+10 ) 
						TABBOOK(o)->Pos += 10;
				}
				TABBOOK(o)->Flags &= ~(TBF_LDWN|TBF_RDWN);
				WidgetDraw(o,NULL);
				return true;
			}
			
			if ( o->Last ) {
				PWidget a = o->Last;
				PWidget b = a;	
				do {
					if ( CURSOR_IN_RECT(TAB(a)->BtCache) ) {
						TabBookSelectTab(TABBOOK(o),TAB(a));
						return true;	
					}		
					a = a->Prev;	
				} while ( a != b );		
			}		
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
PTabBook CreateTabBook ( PApplication App, TRect r ) {
	
	PTabBook o = (PTabBook)malloc(sizeof(TTabBook));
	if ( !o ) return NULL;
	memset(o,0,sizeof(TTabBook));
		
	IntialiseWidget(App, WIDGET(o), r, "TabBook");
	
	WIDGET(o)->Draw = &TabBookDraw;
	WIDGET(o)->EventHandler = &TabBookEventHandler;
	return o;
}
////////////////////////////////////////////////////////////////////////////////
l_bool LibMain (l_text Args)
{
/*
	TRect r;
	PTabBook o;
	
	RectAssign(&r,100,100,300,300);
	
	o = CreateTabBook(&Me,r);
	InsertWidget(WIDGET(desktop),WIDGET(o));
	
	TabBookAddTab(o,"TAB 1",NULL);
	TabBookAddTab(o,"TAB 2",NULL);
	TabBookAddTab(o,"TAB 3",NULL);
	TabBookAddTab(o,"TAB 4",NULL);

	WidgetDrawAll(WIDGET(o));
	
	*/
 APPEXPORT(CreateTabBook);
 APPEXPORT(TabBookAddTab);
 APPEXPORT(TabBookDisposeTab);
 APPEXPORT(TabBookRemoveTab);
 APPEXPORT(TabBookSelectTab);
	
	
	return true;
}
////////////////////////////////////////////////////////////////////////////////

void Close (void)
{


}

