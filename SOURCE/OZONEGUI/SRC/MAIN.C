/**
*	Copyright (c) 2003,2004 Point Mad, Lukas Lipka. All rights reserved.
*
*	FILE:			main.c
*
*	PROJECT:		Phoenix engine - Core
*
*	DESCRIPTION:	The core engine.
*
*	CONTRIBUTORS:
*					Lukas Lipka
*					Julien Etelain
*/
#define KERNEL_FILE

#include "allegro.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "kernel.h"
#include "dynld.h"
#include "internal.h"
#include "dms.h"
#include "widget.h"

#define TSS(x) DebugMessage("Type: %s\tSize: %i %i", #x, sizeof(x),sizeof(x)%8);

/**
*	NAME: GSExit
*	DESCRIPTION: Setting this variable to true ends the main program loop
*		But DO NOT set it directly to true, use the MSG_QUIT event !
*/
extern l_bool GSExit;

/**
*	NAME: KernelMode
*	DESCRIPTION: Defines the kernel mode
*/
l_ulong KernelMode = 0;


/**
*	NAME: Kernel information
*	DESCRIPTION: Information about the kernel itself (version, build)
*/
_PUBLIC char* KernelName		= "Codename: Phoenix Kernel";
_PUBLIC char* KernelCopyright	= "Copyright (c) 2003-2004 Lukas Lipka, Point Mad. All rights reserved.";
_PUBLIC int   KernelVersion		= 500106;
_PUBLIC char* KernelTextVersion	= "0.5.1.6";

/**
*	NAME: System driver initialization
*	DESCRIPTION: The main initialization (systems, drivers, ...)
*/
_PRIVATE void  _SystemInit ( void )
{
	DebugMessage ("Starting driver intialization");

	/*DebugMessage("Starting exception handler...");
		ExceptionHandlerInstall();
	DebugMessage("Exception handler started");*/

	DebugMessage ("Initializing DynLd...");
		InitDynLd();
		RegisterExports();
	DebugMessage ("DynLd initialized");

	DebugMessage ("Initialize VFS...");
		GSSystemVFSInstall();
	DebugMessage ("VFS Initialized");

	DebugMessage("Initializing registry...");
		NRegistryInit();
	DebugMessage("Registry initialized");

	DebugMessage("Initializing text manipulation functions...");
	DebugMessage("	ASCII/UTF-8...");
		InitText();
	DebugMessage("	UTF-16...");
		InitUText();
	DebugMessage("Text manipulation functions initialized");

	DebugMessage ("Initialize Events System...");
		InitializeEventsSystem();
	DebugMessage ("Events System Initialized");

	DebugMessage ("Install Allegro...");
		install_allegro( SYSTEM_AUTODETECT, NULL, NULL );
		install_keyboard();
	DebugMessage ("Allegro installed");

	DebugMessage ("Initialize DMS (Data Management System) ...");
		InitDMS();
	DebugMessage ("DMS initialized");

	DebugMessage ("Initialize basic external drivers...");
		RegistryRunEntries("/SYSTEM/BASICDRIVERS");
	DebugMessage ("External basic drivers initiatlized succesfully");

	DebugMessage ("Install screen...");
		GSSystemScreenInit();
	DebugMessage ("Screen initialized successfully");

	DebugMessage ("Initialize Timer ...");
		InitTimer();
	DebugMessage ("Timer initialized");

	DebugMessage ("Add system fonts...");
		text_mode(-1);
		if ( GSScreenDepth != 8 ) antialias_init(default_palette);
		else antialias_init(NULL);
		default_font = FontLoad(KeyGetText("/SYSTEM/FONTS","SYSTEM/FONTS/HELV13.FNT"));
		if ( !default_font ) default_font = font;
	DebugMessage ("All system fonts succesfully added");

	DebugMessage ("Install keyboard driver...");
		GSSystemKeyboardInstall();
	DebugMessage ("Keyboard driver installed successfully.");

	DebugMessage ("Install mouse driver...");
		GSSystemMouseInstall();
	DebugMessage ("Mouse driver installed successfully.");

	DebugMessage ("Initialize Skinning engine...");
		RegistryRunEntries("/SYSTEM/DRIVERS");
	DebugMessage ("Skinning engine initiatlized succesfully");

	DebugMessage ("Add system widgets...");
		RegistryRunEntries("/SYSTEM/LIBRARIES");
	DebugMessage ("System widgets added succesfully");

	DebugMessage ("Load startup entries...");
		RegistryRunEntries("/SYSTEM/STARTUP");
	DebugMessage ("Desktop initialized");

	/*
	 * Dirty hack, better said the only way
	 */
	{
		void (*func) (void);
		func = (void*)ResolveSymbol(NULL,"DisposeStartupLogo");
		if ( func ) func();
	}

  	MouseShow();

	DebugMessage ("Driver intialization finished\n");
}

/**
*	NAME: System Event Handler
*	DESCRIPTION: System events get handled here.
*/
_PRIVATE void  _SystemEventHandler ( PEvent Event )
{
	if (Event->Type & EV_KEYBOARD)
	{
		if (ALT(Event, KEY_X))
		{
			EventAssign(Event,EV_MESSAGE, MSG_QUIT, NULL, KState, Mouse->State);
			HandleEvent(Event);
			CLEAR_EVENT(Event);
		}

		if (ALT(Event, KEY_F))
		{
			DynLdRun("xapps/nav.app", NULL);
			CLEAR_EVENT(Event);
		}

		if (ALT(Event, KEY_R))
		{
			DynLdRun("xapps/regedit.app", NULL);
			CLEAR_EVENT(Event);
		}

		if (CTRL(Event, KEY_F8))
		{
			p_bitmap shot = create_bitmap(GSScreenWidth, GSScreenHeight);
			blit(screen, shot, 0, 0, 0, 0, GSScreenWidth, GSScreenHeight);
			SaveData(TYPE_IMAGE,shot,"shot.bmp");
			destroy_bitmap(shot);
			CLEAR_EVENT(Event);
		}
		if ( CTRL(Event, KEY_F12) ) {
			p_bitmap shot = create_bitmap(GSScreenWidth, GSScreenHeight);
			
			MouseHide();
			
			if ( shot ) {
				p_bitmap trans = create_bitmap(GSScreenWidth, GSScreenHeight),o=screen;
				
				blit(screen, shot, 0, 0, 0, 0, GSScreenWidth, GSScreenHeight);
				set_clip(screen,0,0,GSScreenWidth, GSScreenHeight);

				if ( trans ) {
	      	set_trans_blender(0, 0, 0, 0);
	      	draw_lit_sprite(trans, shot, 0, 0, 192);
	      	o = trans;
      	}
					
	    	rectfill(o,GSScreenWidth/2-57,GSScreenHeight/2-50,
	    	GSScreenWidth/2-7,GSScreenHeight/2+50,makecol(255,255,255));
	    	
	    	rectfill(o,GSScreenWidth/2+7,GSScreenHeight/2-50,
	    	GSScreenWidth/2+57,GSScreenHeight/2+50,makecol(255,255,255));
	    	
	    	textout_centre(o,default_font,"paused",GSScreenWidth/2+5,GSScreenHeight/2+52,makecol(255,255,255));
	    	textout_centre(o,default_font,"press any key",GSScreenWidth/2+5,GSScreenHeight/2+53+text_height(default_font),makecol(255,255,255));
				
		    if ( trans ) {
			    blit(trans, screen, 0, 0, 0, 0, GSScreenWidth, GSScreenHeight);
					destroy_bitmap(trans);
		    }
			}
			
			while ( !keypressed() ); readkey();
			
			if ( shot ) {
				blit(shot, screen, 0, 0, 0, 0, GSScreenWidth, GSScreenHeight);
				destroy_bitmap(shot);
			}
			
			MouseShow();
			
			CLEAR_EVENT(Event);
		}		
		
	}
}

int main(int argc, char *argv[])
{
	//l_int EventsTheard, TasksTheard;

	/**
	 * Select the kernel mode
	 */
	if ( argc > 1 ) if ( argv[1] ){
		if (!strcmp(argv[1], "-safe")){
			KernelMode = KERNEL_SAFE;
		}
		else if (!strcmp(argv[1], "-resetup")){
			KernelMode = KERNEL_RESETUP;
		}
	}

	/**
	 *	Start logging to kernel.txt
	 */
	GSSystemDebugInstall("kernel.txt");

	DebugMessage("Command: %s", argv[0]);
	
	/**
	 * Install multitasking
	 */
	//ThreadInit();

	/**
	*	Installs the main kernel event handler
	*/
	InstallEventHandler(_SystemEventHandler);

	/**
	 *	Start the main system
	 */
	_SystemInit();

	/**
	 *	Main program loop
	 */
	/*
	DebugMessage("Initialising System Threads");
	EventsTheard = NewThread(EventsThread, NULL, 4096, 1);
	TasksTheard  = NewThread(RunTasks, NULL, 4096, 1);
	DebugMessage("Wait for exit message");
	*/

	while (!GSExit)
	{
		SysPoll();
	}

	//DebugMessage("Uninitialising System Threads");
	//KillThread(EventsTheard);
	//KillThread(TasksTheard);


	/**
	*	Deinitialize the whole system and
	*	shutdown the GUI.
	*/
	ShutDownDynLd(); // Close all applications, and shut down dynamic loader
	ShutDownTimer();
	GSSystemMouseUninstall();
	ShutdownEventsSystem();
	allegro_exit();
	ShutDownDMS();
	//ThreadUnInit();
	NRegistryUnInit();
	//ExceptionHandlerUninstall();
	GSSystemDebugUninstall();

	return 0;
}
END_OF_MAIN();
